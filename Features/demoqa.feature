Feature: Check Elements functionality of demoqa application.

  Scenario: Verify the text box
    Given browser is open
    When user enters full name and email id
    And user enters current address and permenant address
    Then user clicks on submit button

  Scenario: Verify the Check box
    Given user is on check box page
    When user enables the check box
    Then message is displayed

  Scenario: Verify the Radio button
    Given user is on Radio button page
    When user selects radio button
    Then user selected the radio button

  Scenario: Verify the buttons functionality
    Given user is on buttons page
    When user clicks on the click me option
    Then text message is displayed

  Scenario: Verify the links
    Given user is on links page
    When user clicks on link
    Then navigating to respective page

  Scenario: Verify the Broken Links-Images
    Given user is on broken links-images page
    When user clicks on valid link
    Then home page is navigated

  Scenario: Verify upload and download option
    Given user is on upload and download page
    When user click on download
    Then image has been downloaded

  Scenario: Verify the dynamic properties
    Given user is on dynamic properties page
    When user clicks on color change button
    Then verify the color change of text

  Scenario: verify the web table data
    Given user is on web-table page
    When user clicks on add option
    And user enters the required data
    Then user clicks on submit button1
