package StepDefinition;


import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features="D:\\Java\\Framework_BDD\\Features\\demoqa.feature",
glue={"StepDefinition"},

plugin={"summary","pretty",
        "junit:target/MyReports/report.xml",
		"json:target/MyReports/report.json",
		"html:target/MyReports/report.html"},
//        "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"},
monochrome = true
		)
public class TestRuuner {

}
