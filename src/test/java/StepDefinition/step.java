package StepDefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.en.*;

public class step {
	WebDriver driver;

	@Given("browser is open")
	public void browser_is_open() {
		System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to("https://demoqa.com/text-box");

	}

	@When("user enters full name and email id")
	public void user_enters_full_name_and_email_id() throws InterruptedException {
		driver.findElement(By.id("userName")).sendKeys("nmd");
		driver.findElement(By.id("userEmail")).sendKeys("nmd123@gmail.com");
		Thread.sleep(1000);
	}

	@And("user enters current address and permenant address")
	public void user_enters_current_address_and_permenant_address() throws InterruptedException {
		driver.findElement(By.id("currentAddress")).sendKeys("Bilagi");
		driver.findElement(By.id("permanentAddress")).sendKeys("Bengaluru");

		Thread.sleep(1000);
		RemoteWebDriver r= (RemoteWebDriver) driver;
		String c="window.scrollTo(1000,document.body.scrollHeight)";
		r.executeScript(c);
	}

	@Then("user clicks on submit button")
	public void user_clicks_on_submit_button() throws InterruptedException {
		driver.findElement(By.id("submit")).click();
		Thread.sleep(1000);
		driver.close();
	}

	@Given("user is on check box page")
	public void user_is_on_check_box_page() {
		System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to("https://demoqa.com/checkbox");
		//		driver.findElement(By.xpath("//li[@id='item-1'][@class='btn btn-light active']")).click();

	}

	@When("user enables the check box")
	public void user_enables_the_check_box() throws InterruptedException {
		driver.findElement(By.className("rct-title")).click();
		Thread.sleep(1000);
	}

	@Then("message is displayed")
	public void message_is_displayed() {
		boolean e=driver.findElement(By.id("result")).isDisplayed();
		System.out.println(e);
		driver.close();
	}

	@Given("user is on Radio button page")
	public void user_is_on_radio_button_page() {
		System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to("https://demoqa.com/radio-button");
	}
	@When("user selects radio button")
	public void user_selects_radio_button() {
		driver.findElement(By.className("custom-control-label")).click();
	}

	@Then("user selected the radio button")
	public void user_selected_the_radio_button() throws InterruptedException {
		System.out.println("Output has been displayed.....");
		Thread.sleep(2000);
		driver.close();
	}

	@Given("user is on buttons page")
	public void user_is_on_buttons_page() {
		System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to("https://demoqa.com/buttons");
	}

	@When("user clicks on the click me option")
	public void user_clicks_on_the_click_me_option() {
		driver.findElement(By.id("rightClickBtn")).click();
		boolean a=driver.findElement(By.id("rightClickBtn")).isDisplayed();
		System.out.println(a);
	}

	@Then("text message is displayed")
	public void text_message_is_displayed() {
		System.out.println("Text message displayed...");
		driver.close();
	}

	@Given("user is on links page")
	public void user_is_on_links_page() {
		System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demoqa.com/links");
		//		driver.close();
	}

	@When("user clicks on link")
	public void user_clicks_on_link() throws InterruptedException {
		driver.findElement(By.id("simpleLink")).click();
		Thread.sleep(2000);
		driver.close();
	}

	@Then("navigating to respective page")
	public void navigating_to_respective_page() {
		System.out.println("Navigating to home page....");

	}

	@Given("user is on broken links-images page")
	public void user_is_on_broken_links_images_page() {
		System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demoqa.com/broken");
	}

	@When("user clicks on valid link")
	public void user_clicks_on_valid_link() {
		//		driver.findElement(By.cssSelector("a[href='http://demoqa.com']")).click();
//		driver.close();
	}

	@Then("home page is navigated")
	public void home_page_is_navigated() {
		//		boolean b=driver.findElement(By.className("banner-image")).isDisplayed();
		//		System.out.println(b);
		System.out.println("Navigated to home page...");
		driver.close();
	}

	@Given("user is on upload and download page")
	public void user_is_on_upload_and_download_page() {
		System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demoqa.com/upload-download");
	}

	@When("user click on download")
	public void user_click_on_download() throws InterruptedException {
		driver.findElement(By.id("downloadButton")).click();
		//		driver.findElement(By.id("uploadFile")).click();
		RemoteWebDriver q= (RemoteWebDriver) driver;
		String a="window.scrollTo(1000,document.body.scrollHeight)";
		q.executeScript(a);
		Thread.sleep(2000);

	}

	@Then("image has been downloaded")
	public void image_has_been_downloaded() {
		System.out.println("After clicking on download link the image has been downloaded successfully....");
		driver.close();
	}

	@Given("user is on dynamic properties page")
	public void user_is_on_dynamic_properties_page() {
		System.setProperty("wedriver.chrome.driver","./Drivers/chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demoqa.com/dynamic-properties");
	}

	@When("user clicks on color change button")
	public void user_clicks_on_color_change_button() {
		driver.findElement(By.id("colorChange")).click();
	}

	@Then("verify the color change of text")
	public void verify_the_color_change_of_text() {
		System.out.println("color will change after few seconds....");
		driver.close();
	}

	@Given("user is on web-table page")
	public void user_is_on_we_table_page() {
		System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to("https://demoqa.com/webtables");
	}

	@When("user clicks on add option")
	public void user_clicks_on_add_option() {
		driver.findElement(By.id("addNewRecordButton")).click();
	}

	@And("user enters the required data")
	public void user_enters_the_required_data() throws InterruptedException {
		driver.findElement(By.id("firstName")).sendKeys("NMD");
		driver.findElement(By.id("lastName")).sendKeys("AK");
		driver.findElement(By.id("userEmail")).sendKeys("nmd@gmail.com");
		driver.findElement(By.id("age")).sendKeys("20");
		driver.findElement(By.id("salary")).sendKeys("50000");
		driver.findElement(By.id("department")).sendKeys("EEE");
		Thread.sleep(1000);
	}
	@Then("user clicks on submit button1")
	public void user_clicks_on_submit_button1() throws InterruptedException{
		driver.findElement(By.id("submit")).click();
		RemoteWebDriver p= (RemoteWebDriver) driver;
		String d="window.scrollTo(1000,document.body.scrollHeight)";
		p.executeScript(d);
		Thread.sleep(2000);

		driver.quit();
	}

}
